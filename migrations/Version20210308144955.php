<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210308144955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('DROP INDEX IDX_64C19C1727ACA70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__category AS SELECT id, parent_id, name, description FROM category');
        $this->addSql('DROP TABLE category');
        $this->addSql('CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, parent_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, description CLOB DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO category (id, parent_id, name, description) SELECT id, parent_id, name, description FROM __temp__category');
        $this->addSql('DROP TABLE __temp__category');
        $this->addSql('CREATE INDEX IDX_64C19C1727ACA70 ON category (parent_id)');
        $this->addSql('DROP INDEX UNIQ_CAC822D95688DED7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__price AS SELECT id, price_list_id, net, gross, tax FROM price');
        $this->addSql('DROP TABLE price');
        $this->addSql('CREATE TABLE price (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, price_list_id INTEGER DEFAULT NULL, net INTEGER NOT NULL, gross INTEGER NOT NULL, tax INTEGER NOT NULL, CONSTRAINT FK_CAC822D95688DED7 FOREIGN KEY (price_list_id) REFERENCES price_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO price (id, price_list_id, net, gross, tax) SELECT id, price_list_id, net, gross, tax FROM __temp__price');
        $this->addSql('DROP TABLE __temp__price');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CAC822D95688DED7 ON price (price_list_id)');
        $this->addSql('DROP INDEX UNIQ_399A0AA2D614C7E7');
        $this->addSql('DROP INDEX IDX_399A0AA24584665A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__price_list AS SELECT id, product_id, price_id, name, description, type FROM price_list');
        $this->addSql('DROP TABLE price_list');
        $this->addSql('CREATE TABLE price_list (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER NOT NULL, price_id INTEGER DEFAULT NULL, name VARCHAR(255) DEFAULT NULL COLLATE BINARY, description CLOB DEFAULT NULL COLLATE BINARY, type VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_399A0AA24584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_399A0AA2D614C7E7 FOREIGN KEY (price_id) REFERENCES price (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO price_list (id, product_id, price_id, name, description, type) SELECT id, product_id, price_id, name, description, type FROM __temp__price_list');
        $this->addSql('DROP TABLE __temp__price_list');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399A0AA2D614C7E7 ON price_list (price_id)');
        $this->addSql('CREATE INDEX IDX_399A0AA24584665A ON price_list (product_id)');
        $this->addSql('DROP INDEX IDX_D34A04AD12469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__product AS SELECT id, category_id, name, description, ean FROM product');
        $this->addSql('DROP TABLE product');
        $this->addSql('CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, description CLOB DEFAULT NULL COLLATE BINARY, ean VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO product (id, category_id, name, description, ean) SELECT id, category_id, name, description, ean FROM __temp__product');
        $this->addSql('DROP TABLE __temp__product');
        $this->addSql('CREATE INDEX IDX_D34A04AD12469DE2 ON product (category_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX IDX_64C19C1727ACA70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__category AS SELECT id, parent_id, name, description FROM category');
        $this->addSql('DROP TABLE category');
        $this->addSql('CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, parent_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, description CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO category (id, parent_id, name, description) SELECT id, parent_id, name, description FROM __temp__category');
        $this->addSql('DROP TABLE __temp__category');
        $this->addSql('CREATE INDEX IDX_64C19C1727ACA70 ON category (parent_id)');
        $this->addSql('DROP INDEX UNIQ_CAC822D95688DED7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__price AS SELECT id, price_list_id, net, gross, tax FROM price');
        $this->addSql('DROP TABLE price');
        $this->addSql('CREATE TABLE price (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, price_list_id INTEGER DEFAULT NULL, net INTEGER NOT NULL, gross INTEGER NOT NULL, tax INTEGER NOT NULL)');
        $this->addSql('INSERT INTO price (id, price_list_id, net, gross, tax) SELECT id, price_list_id, net, gross, tax FROM __temp__price');
        $this->addSql('DROP TABLE __temp__price');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CAC822D95688DED7 ON price (price_list_id)');
        $this->addSql('DROP INDEX IDX_399A0AA24584665A');
        $this->addSql('DROP INDEX UNIQ_399A0AA2D614C7E7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__price_list AS SELECT id, product_id, price_id, name, description, type FROM price_list');
        $this->addSql('DROP TABLE price_list');
        $this->addSql('CREATE TABLE price_list (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, product_id INTEGER NOT NULL, price_id INTEGER DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, description CLOB DEFAULT NULL, type VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO price_list (id, product_id, price_id, name, description, type) SELECT id, product_id, price_id, name, description, type FROM __temp__price_list');
        $this->addSql('DROP TABLE __temp__price_list');
        $this->addSql('CREATE INDEX IDX_399A0AA24584665A ON price_list (product_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399A0AA2D614C7E7 ON price_list (price_id)');
        $this->addSql('DROP INDEX IDX_D34A04AD12469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__product AS SELECT id, category_id, name, description, ean FROM product');
        $this->addSql('DROP TABLE product');
        $this->addSql('CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, description CLOB DEFAULT NULL, ean VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO product (id, category_id, name, description, ean) SELECT id, category_id, name, description, ean FROM __temp__product');
        $this->addSql('DROP TABLE __temp__product');
        $this->addSql('CREATE INDEX IDX_D34A04AD12469DE2 ON product (category_id)');
    }
}
