<?php

declare(strict_types=1);

namespace App\GraphQL\Definition;

use Doctrine\Common\Collections\Expr\Comparison;
use ReflectionClass;

class GraphDoctrineDefinition
{
    public const IN = Comparison::IN;
    public const NOT_IN = Comparison::NIN;
    public const NOT = Comparison::NEQ;
    public const CONTAINS = Comparison::CONTAINS;
    public const LT = Comparison::LT;
    public const LTE = Comparison::LTE;
    public const GT = Comparison::GT;
    public const GTE = Comparison::GTE;

    public static function getDefinition(string $definition): string
    {
        $reflection = new ReflectionClass(__CLASS__);

        return $reflection->getConstant(mb_strtoupper($definition));
    }
}