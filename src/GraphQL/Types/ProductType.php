<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Entity\PriceList;
use App\GraphQL\Helper\FieldConfigHelper;
use App\GraphQL\Resolver\TypeResolver;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class ProductType extends ObjectType
{
    public function __construct(TypeResolver $resolver)
    {
        $priceListsConfig = FieldConfigHelper::createConfig(
            'priceLists',
            PriceListType::class,
            $resolver,
            PriceList::class
        );

        $config = [
            'name' => 'Product'.md5(microtime().rand(1,10000)),
            'args' => [
                'limit' => [
                    'type' => Type::int(),
                    'defaultValue' => 10
                ]
            ],
            'description' => 'Product object',
            'fields' => [
                'id' => Type::id(),
                'name' => Type::string(),
                'description' => Type::getNullableType(Type::string()),
                'ean' => Type::string(),
                'category' => Type::getNullableType(new CategoryWithoutParentType($resolver)),
                'priceLists' => $priceListsConfig,
            ],
        ];

        parent::__construct($config);
    }
}