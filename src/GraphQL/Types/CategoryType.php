<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Entity\Category;
use App\Entity\Product;
use App\GraphQL\Helper\FieldConfigHelper;
use App\GraphQL\Resolver\TypeResolver;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class CategoryType extends ObjectType implements GraphQLType
{
    public function __construct(TypeResolver $resolver)
    {
        $productsConfig = FieldConfigHelper::createConfig(
            'products',
            ProductWithoutCategoryType::class,
            $resolver,
            Product::class
        );

        $categoriesConfig = FieldConfigHelper::createConfig(
            'categories',
            CategoryWithoutParentType::class,
            $resolver,
            Category::class
        );

        $config = [
            'name' => 'Category'.md5(microtime().rand(1,10000)),
            'description' => 'Category object',
            'fields' => [
                'id' => Type::id(),
                'name' => Type::string(),
                'description' => Type::getNullableType(Type::string()),
                'parent' => Type::getNullableType($this),
                'categories' => $categoriesConfig,
                'products' => $productsConfig,
            ],
        ];

        parent::__construct($config);
    }
}
