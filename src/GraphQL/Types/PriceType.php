<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class PriceType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'Price'.md5(microtime().rand(1,10000)),
            'description' => 'Price list object',
            'fields' => [
                'id' => Type::id(),
                'net' => Type::int(),
                'gross' => Type::int(),
                'tax' => Type::int(),
            ],
        ];

        parent::__construct($config);
    }
}
