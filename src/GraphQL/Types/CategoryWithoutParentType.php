<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\GraphQL\Resolver\TypeResolver;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class CategoryWithoutParentType extends ObjectType implements GraphQLType
{
    public function __construct(TypeResolver $resolver)
    {
        $config = [
            'name' => 'Category'.md5(microtime().rand(1,10000)),
            'description' => 'Category object',
            'fields' => [
                'id' => Type::id(),
                'name' => Type::string(),
                'description' => Type::getNullableType(Type::string()),
                'categories' => Type::listOf($this),
                'products' => Type::listOf(new ProductWithoutCategoryType($resolver)),
            ],
        ];

        parent::__construct($config);
    }
}
