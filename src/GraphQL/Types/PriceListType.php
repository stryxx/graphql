<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class PriceListType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'PriceList'.md5(microtime().rand(1,10000)),
            'description' => 'Price list object',
            'fields' => [
                'id' => Type::id(),
                'name' => Type::string(),
                'description' => Type::getNullableType(Type::string()),
                'type' => Type::string(),
                'price' => new PriceType(),
            ],
        ];

        parent::__construct($config);
    }
}
