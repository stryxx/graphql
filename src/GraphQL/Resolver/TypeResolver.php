<?php

declare(strict_types=1);

namespace App\GraphQL\Resolver;

use App\Entity\FieldGetter;
use App\GraphQL\Helper\ArgsHelper;
use Doctrine\ORM\EntityManagerInterface;

class TypeResolver
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function resolve(string $fieldName, string $entityClass, array $args, array $root): array
    {
        ['args' => $args, 'field' => $field, 'condition' => $condition] = ArgsHelper::resolve($args);

        $results = $this->entityManager->getRepository($entityClass)->findAllByArgs(
            $args,
            $field,
            $condition,
            ['field' => $root[$fieldName]['parentField'], 'id' => $root[$fieldName]['parentId'],]
        );
        $returned = [];
        /** @var FieldGetter $result */
        foreach ($results as $key => $result) {
            $returned[$key] = $result->getFields($root[$fieldName]['fields']);
        }

        return $returned;
    }
}
