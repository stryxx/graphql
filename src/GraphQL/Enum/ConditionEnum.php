<?php

declare(strict_types=1);

namespace App\GraphQL\Enum;

class ConditionEnum
{
    public const IN = 'in';
    public const NOT_IN = 'not_in';
    public const NOT = 'not';
    public const CONTAINS = 'contains';
    public const LT = 'lt';
    public const LTE = 'lte';
    public const GT = 'gt';
    public const GTE = 'gte';
}
