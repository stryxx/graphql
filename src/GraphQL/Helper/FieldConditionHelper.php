<?php

declare(strict_types=1);

namespace App\GraphQL\Helper;

use App\GraphQL\Types\GraphQLType;
use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\StringType;
use GraphQL\Type\Definition\Type;

class FieldConditionHelper
{
    public static function getConditionalFields(string $fieldName, Type|array $type): array
    {
        $type = is_array($type) ? $type['type'] : $type;
        $conditionalFields = [];
        $conditionalFields += match (true) {
            $type instanceof StringType => [
                $fieldName . GraphQLType::CONTAINS => $type,
            ],
            $type instanceof IntType, $type instanceof IDType => [
                $fieldName . GraphQLType::LESS_THAN => $type,
                $fieldName . GraphQLType::LESS_THAN_EQUAL => $type,
                $fieldName . GraphQLType::GREATER_THAN => $type,
                $fieldName . GraphQLType::GREATER_THAN_EQUAL => $type,
            ],
            default => []
        };


        $conditionalFields += [
            $fieldName . GraphQLType::IN => Type::listOf($type),
            $fieldName . GraphQLType::NOT_IN => Type::listOf($type),
            $fieldName . GraphQLType::NOT => $type,
        ];

        return $conditionalFields;
    }
}
