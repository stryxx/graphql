<?php

declare(strict_types=1);

namespace App\GraphQL\Helper;

use App\GraphQL\Resolver\TypeResolver;
use GraphQL\Type\Definition\Type;
use ReflectionClass;

class FieldConfigHelper
{
    public static function createConfig(string $fieldName, string $typeClass, TypeResolver $resolver, string $entityClass): array
    {
        $reflection = new ReflectionClass($typeClass);
        $parameters = $reflection->getConstructor()->getParameters();
        if (empty($parameters)) {
            $instance = new $typeClass();
        } else {
            $instance = new $typeClass($resolver);
        }

        $config = [
            'type' => Type::listOf($instance),
            'resolve' => function(array $root, array $args) use ($resolver, $entityClass, $fieldName) {
                return $resolver->resolve($fieldName, $entityClass, $args, $root);
            },
            'args' => [
                'limit' => [
                    'type' => Type::int(),
                    'defaultValue' => 10,
                ],
            ],
        ];

        return ArgsHelper::conditionFields($instance, $config);
    }
}