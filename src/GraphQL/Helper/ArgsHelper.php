<?php

declare(strict_types=1);

namespace App\GraphQL\Helper;

use GraphQL\Type\Definition\Type;

class ArgsHelper
{
    public static function resolve(array $args): array
    {
        $limit = $args['limit'];
        unset($args['limit']);
        $fieldData = explode('_', key($args) ?: '');
        if (false === empty($fieldData)) {
            $field = $fieldData[0];
            $condition = isset($fieldData[1]) ? join('_', array_splice($fieldData, 1)) : null;
            $args['limit'] = $limit;
        }

        return [
            'args' => $args,
            'field' => $field ?? null,
            'condition' => $condition ?? null,
        ];
    }

    public static function conditionFields(Type $instance, array $config): array
    {
        foreach ($instance->config['fields'] as $fieldName => $field) {
            $config['args'][$fieldName] = $field;
            $config['args'] += FieldConditionHelper::getConditionalFields($fieldName, $field);
        }

        return $config;
    }
}
