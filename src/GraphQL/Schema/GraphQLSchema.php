<?php

declare(strict_types=1);

namespace App\GraphQL\Schema;

use App\Entity\FieldGetter;
use App\GraphQL\Helper\ArgsHelper;
use App\GraphQL\Helper\FieldConditionHelper;
use App\Repository\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;

class GraphQLSchema
{
    private Schema $schema;

    public function __construct(iterable $types, EntityManagerInterface $entityManager)
    {
        $this->schema = new Schema([
            'query' => new ObjectType([
                'name' => 'Query',
                'fields' => $this->getFields(
                    $this->iterableToArray($types),
                    $entityManager
                )
            ]),
        ]);
    }

    private function getFields(array $types, EntityManagerInterface $entityManager): array
    {
        $fields = [];
        $repositories = [];

        /**
         * @var string $typeName
         * @var Type $type
         */
        foreach ($this->iterableToArray($types) as $typeName => $type) {
            $fields[$typeName] = [
                'type' => Type::listOf($type),
                'resolve' => function (?array $root, array $args, $null, ResolveInfo $resolveInfo) use ($entityManager, $repositories) {
                    $entityNamespace = $entityManager
                        ->getConfiguration()
                        ->getEntityNamespace('App') . '\\';
                    $fields = $resolveInfo->getFieldSelection(3);
                    /** @var ObjectRepository $repository */
                    $repository = $entityManager->getRepository(
                        $entityNamespace . ucfirst($resolveInfo->fieldName)
                    );

                    ['args' => $args, 'field' => $field, 'condition' => $condition] = ArgsHelper::resolve($args);

                    $results = $repository->findAllByArgs($args, $field, $condition);

                    $returned = [];
                    /** @var FieldGetter $result */
                    foreach ($results as $key => $result) {
                        $returned[$key] = $result->getFields($fields);
                    }

                    return $returned;
                }
            ];

            $fields[$typeName]['args'] = [
                'limit' => [
                    'type' => Type::int(),
                    'defaultValue' => 10,
                ],
            ];
            foreach ($type->config['fields'] as $fieldName => $field) {
                $fields[$typeName]['args'][$fieldName] = $field;
                $fields[$typeName]['args'] += FieldConditionHelper::getConditionalFields($fieldName, $field);
            }
        }

        return $fields;
    }

    private function iterableToArray(iterable $types): array
    {
        $array = [];
        /** @var Type $type */
        foreach ($types as $type) {
            $array[$this->getName($type)] = $type;
        }

        return $array;
    }

    private function getName(Type $type): string
    {
        $className = $type::class;

        return mb_strtolower(mb_substr($className, mb_strrpos($className, '\\') + 1, -4));
    }

    public function getSchema(): Schema
    {
        return $this->schema;
    }
}
