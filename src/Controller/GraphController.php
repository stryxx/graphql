<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonDecode;

class GraphController extends AbstractController
{
    #[Route('/graph', name: 'app.graph')]
    public function graph(Query $query, Request $request): Response
    {
        if (false === empty($request->getContent())) {
            $jsonDecoder = new JsonDecode();
            $graphQuery = $jsonDecoder->decode($request->getContent(), 'json', [JsonDecode::ASSOCIATIVE => true]);
        } else {
            $graphQuery['query'] = $request->get('query');
        }

        return new JsonResponse($query->runQuery($graphQuery['query'] ?? ''));
    }
}
