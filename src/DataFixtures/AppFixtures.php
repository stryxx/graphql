<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Price;
use App\Entity\PriceList;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /** @var Connection $connection */
        $connection = $manager->getConnection();
        $connection->executeQuery("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='product'");
        $connection->executeQuery("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='category'");
        $connection->executeQuery("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='price'");
        $connection->executeQuery("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='price_list'");
        try {
            $productsCount = random_int(20, 40);
            $categoriesCount = random_int(5, 10);

            $faker = Factory::create();

            $categories = new ArrayCollection();
            for ($i = 0; $i < $categoriesCount; $i++) {
                $category = new Category();
                $category->setName($faker->word);
                $category->setDescription($faker->sentences(6, true));
                $manager->persist($category);
                $categories->add($category);
            }

            $products = new ArrayCollection();
            for ($i = 0; $i < $productsCount; $i++) {
                $product = new Product();
                $product->setName($faker->sentence);
                $product->setDescription($faker->sentences(6, true));
                $product->setCategory($categories->get(random_int(0, $categoriesCount - 1)));
                $product->setEan($faker->ean13);
                $manager->persist($product);
                $products->add($product);
            }

            foreach ($products as $product) {
                $prices = random_int(1, 3);
                switch ($prices) {
                    case 1:
                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);
                        $price = new Price();
                        $price->setTax($tax);
                        $price->setNet($net);
                        $price->setGross($net * (1 + ($tax/100)));

                        $priceList = new PriceList();
                        $priceList->setType('regular');
                        $priceList->setPrice($price);
                        $priceList->setProduct($product);
                        $priceList->setDescription($faker->words(6, true));
                        $price->setPriceList($priceList);

                        $manager->persist($price);
                        $manager->persist($priceList);
                        $manager->persist($product);
                        break;
                    case 2:
                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);

                        $price1 = new Price();
                        $price1->setTax($tax);
                        $price1->setNet($net);
                        $price1->setGross($net * (1 + ($tax/100)));

                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);

                        $price2 = new Price();
                        $price2->setTax($tax);
                        $price2->setNet($net);
                        $price2->setGross($net * (1 + ($tax/100)));

                        $priceList1 = new PriceList();
                        $priceList1->setType('regular');
                        $priceList1->setPrice($price1);
                        $priceList1->setProduct($product);
                        $priceList1->setDescription($faker->words(6, true));
                        $price1->setPriceList($priceList1);

                        $priceList2 = new PriceList();
                        $priceList2->setType('promotional');
                        $priceList2->setPrice($price2);
                        $priceList2->setProduct($product);
                        $priceList2->setDescription($faker->words(6, true));
                        $price2->setPriceList($priceList2);

                        $manager->persist($price1);
                        $manager->persist($price2);
                        $manager->persist($priceList1);
                        $manager->persist($priceList2);
                        $manager->persist($product);
                        break;
                    case 3:
                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);

                        $price1 = new Price();
                        $price1->setTax($tax);
                        $price1->setNet($net);
                        $price1->setGross($net * (1 + ($tax/100)));

                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);

                        $price2 = new Price();
                        $price2->setTax($tax);
                        $price2->setNet($net);
                        $price2->setGross($net * (1 + ($tax/100)));

                        $net = $faker->numberBetween(1000, 9999);
                        $tax = $faker->randomElement([5, 8, 23]);

                        $price3 = new Price();
                        $price3->setTax($tax);
                        $price3->setNet($net);
                        $price3->setGross($net * (1 + ($tax/100)));

                        $priceList1 = new PriceList();
                        $priceList1->setType('regular');
                        $priceList1->setPrice($price1);
                        $priceList1->setProduct($product);
                        $priceList1->setDescription($faker->words(6, true));
                        $price1->setPriceList($priceList1);

                        $priceList2 = new PriceList();
                        $priceList2->setType('promotional');
                        $priceList2->setPrice($price2);
                        $priceList2->setProduct($product);
                        $priceList2->setDescription($faker->words(6, true));
                        $price2->setPriceList($priceList2);

                        $priceList3 = new PriceList();
                        $priceList3->setType('sale');
                        $priceList3->setPrice($price3);
                        $priceList3->setProduct($product);
                        $priceList3->setDescription($faker->words(6, true));
                        $price3->setPriceList($priceList3);

                        $manager->persist($price1);
                        $manager->persist($price2);
                        $manager->persist($price3);
                        $manager->persist($priceList1);
                        $manager->persist($priceList2);
                        $manager->persist($priceList3);
                        $manager->persist($product);
                        break;
                }
            }

            $manager->flush();

        } catch (\Exception $e) {
            echo $e->getMessage();
            return;
        }
    }
}
