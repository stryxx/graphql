<?php

declare(strict_types=1);

namespace App\Repository;

use App\GraphQL\Definition\GraphDoctrineDefinition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;

abstract class ObjectRepository extends ServiceEntityRepository
{
    public function findAllByArgs(array $args, ?string $field = null, ?string $condition = null, array $parent = []): array
    {
        $queryBuilder = $this->createQueryBuilder('o');

        $queryBuilder->setMaxResults($args['limit']);
        unset($args['limit']);

        $value = current($args);

        if (false === empty($parent)) {
            $queryBuilder->addCriteria(
                new Criteria(new Comparison($parent['field'], Comparison::EQ, $parent['id']))
            );
        }

        if ($condition) {
            $queryBuilder->addCriteria(
                new Criteria(new Comparison($field, GraphDoctrineDefinition::getDefinition($condition), $value))
            );

            return $queryBuilder->getQuery()->getResult();
        }

        return $queryBuilder->getQuery()->getResult();
    }
}