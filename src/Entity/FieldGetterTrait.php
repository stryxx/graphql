<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

trait FieldGetterTrait
{
    public function getFields(array $fields): array
    {
        $result = [];
        foreach ($fields as $fieldName => $value) {
            if (is_array($value)) {
                if ($this->$fieldName instanceof Collection) {
                    $result[$fieldName] = [
                        'fields' => $value,
                        'parentId' => $this->id,
                        'parentField' => strtolower(substr($this::class, strrpos($this::class, '\\') + 1)),
                    ];
                } elseif (is_object($this->$fieldName)) {
                    $result[$fieldName] = $this->$fieldName->getFields($value);
                }
            } else {
                $result[$fieldName] = $this->$fieldName;
            }
        }

        return $result;
    }
}
