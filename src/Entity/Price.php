<?php

declare(strict_types=1);

namespace App\Entity;

use App\GraphQL\Model\GraphTypeInterface;
use App\Repository\PriceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PriceRepository::class)
 */
class Price implements FieldGetter, GraphTypeInterface
{
    use FieldGetterTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity=PriceList::class, inversedBy="price", cascade={"persist", "remove"})
     */
    private ?PriceList $priceList;

    /**
     * @ORM\Column(type="integer")
     */
    private int $net;

    /**
     * @ORM\Column(type="integer")
     */
    private int $gross;

    /**
     * @ORM\Column(type="integer")
     */
    private int $tax;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriceList(): ?PriceList
    {
        return $this->priceList;
    }

    public function setPriceList(?PriceList $priceList): self
    {
        $this->priceList = $priceList;

        return $this;
    }

    public function getNet(): ?int
    {
        return $this->net;
    }

    public function setNet(int $net): self
    {
        $this->net = $net;

        return $this;
    }

    public function getGross(): ?int
    {
        return $this->gross;
    }

    public function setGross(int $gross): self
    {
        $this->gross = $gross;

        return $this;
    }

    public function getTax(): ?int
    {
        return $this->tax;
    }

    public function setTax(int $tax): self
    {
        $this->tax = $tax;

        return $this;
    }
}
