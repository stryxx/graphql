<?php

declare(strict_types=1);

namespace App\Entity;

interface FieldGetter
{
    public function getFields(array $fields): array;
}