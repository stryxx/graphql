<?php

declare(strict_types=1);

namespace App\Service;

use App\GraphQL\Schema\GraphQLSchema;
use GraphQL\GraphQL;

class Query
{
    private GraphQLSchema $schema;

    public function __construct(GraphQLSchema $schema)
    {
        $this->schema = $schema;
    }

    public function runQuery(string $query): array
    {
        try {
            $result = GraphQL::executeQuery($this->schema->getSchema(), $query);
        } catch (\Throwable $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }

        return $result->toArray();
    }
}